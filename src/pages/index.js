import React from 'react'
import { DateTime } from 'luxon'
import { orderBy } from 'lodash'
import LoadableImage from '../components/LoadableImage'
import Layout from '../components/Layout'
import classes from './index.module.css'

const badgeStyles = {
  default: {
    color: '#212529',
    backgroundColor: '#eeeeee',
    border: '1px solid #212529',
  },
  Morning: {
    color: '#212529',
    backgroundColor: '#d0e0fe',
    border: '1px solid #212529',
  },
  Lunch: {
    color: '#212529',
    backgroundColor: '#fedde5',
    border: '1px solid #212529',
  },
  Afternoon: {
    color: '#212529',
    backgroundColor: '#ede3fd',
    border: '1px solid #212529',
  },
}

const todOrder = ['Afternoon', 'Lunch', 'Morning']
const todSort = e => todOrder.indexOf(e.node.time_of_day)

const IndexPage = ({ data }) => {
  const sorted = orderBy(data.files.group, 'fieldValue', 'desc')
  let imgCount = 0
  return (
    <Layout>
      <div>
        <h1 className="mb-1">
          HungryAzel<small className="text-muted">.club</small>
        </h1>
        <h5 className="mb-4">
          <small className="text-muted">
            <a href="https://submit.hungryazel.club/">
              Submit at submit.hungryazel.club
            </a>
          </small>
        </h5>
        {sorted.map(s => {
          const title = DateTime.fromFormat(s.fieldValue, 'yyyy-MM-dd')
          const sortedEdges = orderBy(
            s.edges,
            [todSort, 'node.filename'],
            ['asc', 'desc']
          )
          return (
            <div key={s.fieldValue}>
              <h2 className="mb-3">
                {title.toLocaleString({
                  ...DateTime.DATE_FULL,
                  weekday: 'long',
                })}
              </h2>
              <div className={classes.dayimages}>
                {sortedEdges.map((e, ei) => {
                  const { node } = e
                  const uri =
                    `data:image/svg+xml;base64,` +
                    (typeof Buffer !== 'undefined'
                      ? Buffer.from(node.sqip).toString('base64')
                      : btoa(node.sqip))
                  imgCount++
                  return (
                    <div className="card bg-dark text-white" key={ei}>
                      <div className={classes.outer}>
                        <LoadableImage
                          placeholder={uri}
                          src={node.url}
                          initiallyVisible={imgCount < 10}
                        />
                      </div>
                      <div className="card-img-overlay p-2">
                        <h5 className="card-title">
                          <span
                            className="badge"
                            style={
                              badgeStyles[node.time_of_day] ||
                              badgeStyles.default
                            }
                          >
                            {node.time_of_day}
                          </span>
                        </h5>
                        {node.quantity && node.quantity > 1 ? (
                          <h5
                            className={
                              'm-2 position-absolute ' + classes.quantity
                            }
                          >
                            <span className="badge badge-dark p-1">
                              {node.quantity}x
                            </span>
                          </h5>
                        ) : null}
                      </div>
                    </div>
                  )
                })}
              </div>
            </div>
          )
        })}
      </div>
    </Layout>
  )
}

export default IndexPage

export const query = graphql`
  query InfoQuery {
    files: allAirtablefiles {
      group(field: date) {
        fieldValue
        edges {
          node {
            filename
            url
            date
            time_of_day
            sqip
            quantity
          }
        }
      }
    }
  }
`
