import React from 'react'
import classes from './LoadableImage.module.css'

export default class LoadableImage extends React.Component {
  state = {
    visible: this.props.initiallyVisible || false,
  }

  node = React.createRef()

  componentDidMount() {
    if (this.state.visible) return

    const watcher = new window.IntersectionObserver(
      ([change]) => {
        if (change.intersectionRatio > 0) {
          watcher.disconnect()
          this.watcher = null
          this.setState({ visible: true })
        }
      },
      {
        rootMargin: '50% 0px',
        threshold: 0,
      }
    )
    watcher.observe(this.node.current)
    this.watcher = watcher
  }

  componentWillUnmount() {
    if (this.watcher) {
      this.watcher.disconnect()
    }
  }

  render() {
    return (
      <div
        style={{
          backgroundImage:
            (this.state.visible ? `url('${this.props.src}'), ` : '') +
            `url('${this.props.placeholder}')`,
        }}
        className={classes.inner}
        ref={this.node}
      />
    )
  }
}
