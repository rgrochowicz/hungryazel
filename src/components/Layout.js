import React from 'react'
import Helmet from 'react-helmet'
import 'bootstrap/dist/css/bootstrap.min.css'
import { StaticQuery } from 'gatsby'

const Layout = ({ children, data }) => (
  <div>
    <Helmet>
      <title>{data.site.siteMetadata.title}</title>
      <link rel="icon" href={require('./icon.png')} />
      <meta name="description" content="Azel is very hungry!" />
      <meta name="keywords" content="food, club" />
    </Helmet>
    <div
      style={{
        margin: '0 auto',
        maxWidth: 960,
        padding: '1rem 1.0875rem 1.45rem',
      }}
    >
      {children}
    </div>
  </div>
)

export default ({ children }) => (
  <StaticQuery
    query={graphql`
      query SiteTitleQuery {
        site {
          siteMetadata {
            title
          }
        }
      }
    `}
    render={data => <Layout data={data}>{children}</Layout>}
  />
)
