# HungryAzel site

Here's the site that shows the photos.

As hungryazel.club is not around anymore, the Netlify site is available here: https://wizardly-einstein-d60488.netlify.com/

### Bucket hooks and slack bot forthcoming

## Install

Make sure that you have the Gatsby CLI program installed:

```sh
npm install --global gatsby-cli
```

Then you can run it by:

```sh
gatsby develop
```

## Build

1.  `./build.sh`

## Deploy

Deploys are done with a push to master

## Submission site

Check here: https://gitlab.com/rgrochowicz/hungryazel/tree/submit
