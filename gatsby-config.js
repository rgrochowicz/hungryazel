module.exports = {
  siteMetadata: {
    title: 'Hungry Azel',
  },
  plugins: [
    'gatsby-plugin-react-helmet',
    'gatsby-plugin-no-sourcemaps',
    'gatsby-plugin-netlify',
    {
      resolve: `gatsby-source-airtable`,
      options: {
        apiKey: process.env.AIRTABLE_KEY,
        baseId: `apptoevLpOUkNNKrj`,
        tableName: `files`,
        tableView: `only visible`,
        queryName: 'files',
      },
    },
  ],
}
