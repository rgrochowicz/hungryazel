const Airtable = require('airtable')
const crypto = require(`crypto`)

exports.sourceNodes = async (
  { actions },
  { apiKey, baseId, tableName, tableView, queryName }
) => {
  const { createNode, setPluginStatus } = actions
  const base = new Airtable({
    apiKey,
  }).base(baseId)
  const table = base(tableName)

  const query = await table.select({
    view: tableView,
  })

  const all = await query.all()

  setPluginStatus({
    status: {
      lastFetched: new Date().toJSON(),
    },
  })

  all.forEach(row => {
    const gatsbyNode = Object.assign(
      {
        // Required Gatsby fields
        id: `${row.id}`,
        parent: '__SOURCE__',
        children: [],
        internal: {
          type: `Airtable${queryName}`,
          contentDigest: crypto
            .createHash('md5')
            .update(JSON.stringify(row))
            .digest('hex'),
        },
      },
      row.fields
    )

    createNode(gatsbyNode)
  })
}
